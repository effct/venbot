---
aliases: keybinds, mf
---

If global keybinds and possibly some more features aren't working for you, it's likely you installed the Vencord Desktop app.

Vencord Desktop is a standalone independent app that is missing some features from the regular Discord app.

To fix it, stop using Vencord Desktop and switch to regular Discord patched with Vencord. To do so, head over to https://vencord.dev/download for Download options!

This is well documented in the README, so please pay more attention in the future.
